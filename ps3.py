import math
import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 8

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1,
    'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
}

WORDLIST_FILENAME = "words.txt"


def load_words():
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.append(line.strip().lower())
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def get_frequency_dict(sequence) ->dict:
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x, 0) + 1
    return freq


def get_word_score(word: str, n: int) -> int:
    component_1 = sum([SCRABBLE_LETTER_VALUES.get(x, 0) for x in word.lower()])
    component_2 = max((7*len(word) - 3*(n - len(word))), 1)

    return component_1*component_2


def display_hand(hand: dict):
    for letter in hand.keys():
        for j in range(hand[letter]):
            print(letter, end=' ')
    print()


def deal_hand(n: int) ->dict:
    hand = {}
    num_vowels = int(math.ceil(n / 3))
    hand['*'] = 1

    for i in range(num_vowels-1):
        x = random.choice(VOWELS)
        hand[x] = hand.get(x, 0) + 1

    for i in range(num_vowels, n):
        x = random.choice(CONSONANTS)
        hand[x] = hand.get(x, 0) + 1

    return hand


def update_hand(hand: dict, word: str) ->dict:
    word_letters = get_frequency_dict(word.lower())

    return {x: y-word_letters.get(x, 0) for (x, y) in hand.items() if y > word_letters.get(x, 0)}


def is_valid_word(word: str, hand: dict, word_list) ->bool:
    word = word.lower()

    if '*' in word:
        possible_words = [word.replace('*', x) for x in VOWELS]
    else:
        possible_words = [word]

    return all([word.count(x) <= hand[x] if (x in hand) else False for x in word]) and \
           any([word in word_list for word in possible_words])


def calculate_handlen(hand: dict) ->int:
    return sum(hand.values())


def play_hand(hand: dict, word_list, total=0):
    print('Current Hand: ', end='')
    display_hand(hand)
    word = input("Enter word, or '!!' to indicate that you are finished: ")

    if (word == '!!'):
        print('Total score: ', total)
        return

    if is_valid_word(word, hand, word_list):
        total += get_word_score(word, calculate_handlen(hand))
        print('"', word, '"', 'earned', get_word_score(word, calculate_handlen(hand)), 'Total: ', total, 'points')
    else:
        print('That is not a valid word. Please choose another word.')

    hand = update_hand(hand, word)

    if not(hand):
        print('Ran out of letters. Total score:', total, 'points')
        return

    play_hand(hand, word_list, total)


def substitute_hand(hand: dict, letter: str) ->dict:
    if not(letter in (hand)) or (letter == '*'):
        return hand

    letters = (VOWELS+CONSONANTS).replace(letter, '')
    letters = list(filter(lambda x: not(x in hand), letters))
    new_letter = random.choice(letters)
    hand[new_letter] = hand.pop(letter)

    return hand


def play_game(word_list, n=1):
    if n != 0:
        hand = deal_hand(HAND_SIZE)
        print('Current Hand: ', end='')
        display_hand(hand)

        if input('Would you like to substitute a letter?').lower() == 'yes':
            letter = input('Which letter would you like to replace: ').lower()
            hand = substitute_hand(hand, letter)

        while True:
            play_hand(hand, word_list)
            if input('Would you like to replay the hand? ').lower() == 'no': break

        play_game(word_list, n-1)
    else: pass


if __name__ == '__main__':
    word_list = load_words()
    n = int(input('Enter total number of hands:'))
    play_game(word_list, n)
